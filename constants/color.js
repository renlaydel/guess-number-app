const Color = {
  primary: '#e1ad01',
  secondary: '#cc4400',
  background: '#fffae6',
};

export default Color;
