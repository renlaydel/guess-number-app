import React, { useState } from 'react';
import { View, StyleSheet, Text, TextInput } from 'react-native';
import Color from '../constants/color';
import Card from '../components/Card';
import SecondaryButton from '../components/SecondaryButton';
import PrimaryButton from '../components/PrimaryButton';
import Input from '../components/Input';

const StartGameScreen = (props) => {
  const [number, setNumber] = useState('');

  const numberHandler = (value) => {
    console.log('numberHandler', Number.isNaN(value));
  };
  
  return (
    <View style={styles.screen}>
      <Text style={styles.title}>Start a New Game</Text>
      <Card style={styles.inputContainer}>
        <Text>Select a number</Text>
        <Input
          style={styles.input}
          keyboardType="number-pad"
          maxLength={2}
          onChangeText={numberHandler}
        />
        <View style={styles.buttonContainer}>
          <SecondaryButton title="Reset"/>
          <PrimaryButton title="Confirm"/>
        </View>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    backgroundColor: Color.background,
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    marginVertical: 10,
  },
  inputContainer: {
    width: 300,
    maxWidth: '80%',
    alignItems: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    width: '100%'
  },
  input: {
    width: 30,
    textAlign: 'center',
  },
});

export default StartGameScreen;
