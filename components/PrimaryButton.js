import React from 'react';
import { View, Button, StyleSheet } from 'react-native';
import Color from '../constants/color';

const PrimaryButton = (props) => {
  return (
    <View style={styles.button}>
      <Button title={props.title} color={Color.primary}></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
  },
});

export default PrimaryButton;