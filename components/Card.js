import React from 'react';
import { View, StyleSheet } from 'react-native';

const Card = (props) => {
  const style = {
    ...styles.card,
    ...props.style,
  };
  return (
   <View style={style}>
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    shadowColor: 'black',
    shadowOpacity: 0.20,
    shadowRadius: 6,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    elevation: 5,
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 5,
  },
});

export default Card;