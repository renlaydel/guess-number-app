import React from 'react';
import { View, Button, StyleSheet } from 'react-native';
import Color from '../constants/color';

const SecondaryButton = (props) => {
  return (
    <View style={styles.button}>
      <Button title={props.title} color={Color.secondary}></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
  },
});

export default SecondaryButton;