import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Color from '../constants/color';

const Header = (props) => {
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>
        {props.title}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 80,
    paddingTop: 36,
    backgroundColor: Color.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: 'black',
    fontWeight: '500',
    fontSize: 20, 
  },
});

export default Header;